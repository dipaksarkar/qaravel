import { Q as QPage } from "./QPage.d14d1fc7.js";
import { _ as _export_sfc, U as createBlock, V as withCtx, S as openBlock, W as createBaseVNode } from "./index.94c1c68b.js";
const _sfc_main = {};
const _hoisted_1 = /* @__PURE__ */ createBaseVNode("div", {
  class: "row",
  style: { "width": "300px" }
}, [
  /* @__PURE__ */ createBaseVNode("div", { class: "text-h5" }, "Dashboard"),
  /* @__PURE__ */ createBaseVNode("div", { class: "sub-title" }, "This page is blank, content is currently being developed.")
], -1);
function _sfc_render(_ctx, _cache) {
  return openBlock(), createBlock(QPage, {
    padding: "",
    class: "flex flex-center"
  }, {
    default: withCtx(() => [
      _hoisted_1
    ]),
    _: 1
  });
}
var IndexPage = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "IndexPage.vue"]]);
export { IndexPage as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiSW5kZXhQYWdlLmY2MjIxZTI0LmpzIiwic291cmNlcyI6W10sInNvdXJjZXNDb250ZW50IjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7In0=
