import { _ as _export_sfc, bu as defineComponent, X as createElementBlock, W as createBaseVNode, d as createVNode, S as openBlock, a2 as QBtn } from "./index.94c1c68b.js";
const _sfc_main = defineComponent({
  name: "ErrorNotFound"
});
const _hoisted_1 = { class: "fullscreen bg-blue text-white text-center q-pa-md flex flex-center" };
const _hoisted_2 = /* @__PURE__ */ createBaseVNode("div", { style: { "font-size": "30vh" } }, "404", -1);
const _hoisted_3 = /* @__PURE__ */ createBaseVNode("div", {
  class: "text-h2",
  style: { "opacity": "0.4" }
}, "Oops. Nothing here...", -1);
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  return openBlock(), createElementBlock("div", _hoisted_1, [
    createBaseVNode("div", null, [
      _hoisted_2,
      _hoisted_3,
      createVNode(QBtn, {
        class: "q-mt-xl",
        color: "white",
        "text-color": "blue",
        unelevated: "",
        to: "/",
        label: "Go Home",
        "no-caps": ""
      })
    ])
  ]);
}
var ErrorNotFound = /* @__PURE__ */ _export_sfc(_sfc_main, [["render", _sfc_render], ["__file", "ErrorNotFound.vue"]]);
export { ErrorNotFound as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiRXJyb3JOb3RGb3VuZC5iODNkYTM1YS5qcyIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3BhZ2VzL0Vycm9yTm90Rm91bmQudnVlIl0sInNvdXJjZXNDb250ZW50IjpbIjx0ZW1wbGF0ZT5cbiAgPGRpdlxuICAgIGNsYXNzPVwiZnVsbHNjcmVlbiBiZy1ibHVlIHRleHQtd2hpdGUgdGV4dC1jZW50ZXIgcS1wYS1tZCBmbGV4IGZsZXgtY2VudGVyXCJcbiAgPlxuICAgIDxkaXY+XG4gICAgICA8ZGl2IHN0eWxlPVwiZm9udC1zaXplOiAzMHZoXCI+NDA0PC9kaXY+XG4gICAgICA8ZGl2IGNsYXNzPVwidGV4dC1oMlwiIHN0eWxlPVwib3BhY2l0eTogMC40XCI+T29wcy4gTm90aGluZyBoZXJlLi4uPC9kaXY+XG4gICAgICA8cS1idG5cbiAgICAgICAgY2xhc3M9XCJxLW10LXhsXCJcbiAgICAgICAgY29sb3I9XCJ3aGl0ZVwiXG4gICAgICAgIHRleHQtY29sb3I9XCJibHVlXCJcbiAgICAgICAgdW5lbGV2YXRlZFxuICAgICAgICB0bz1cIi9cIlxuICAgICAgICBsYWJlbD1cIkdvIEhvbWVcIlxuICAgICAgICBuby1jYXBzXG4gICAgICAvPlxuICAgIDwvZGl2PlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgeyBkZWZpbmVDb21wb25lbnQgfSBmcm9tIFwidnVlXCI7XG5cbmV4cG9ydCBkZWZhdWx0IGRlZmluZUNvbXBvbmVudCh7XG4gIG5hbWU6IFwiRXJyb3JOb3RGb3VuZFwiLFxufSk7XG48L3NjcmlwdD5cbiJdLCJuYW1lcyI6WyJfY3JlYXRlRWxlbWVudFZOb2RlIiwiX29wZW5CbG9jayIsIl9jcmVhdGVFbGVtZW50QmxvY2siLCJfY3JlYXRlVk5vZGUiXSwibWFwcGluZ3MiOiI7QUF1QkEsTUFBSyxZQUFhLGdCQUFhO0FBQUEsRUFDN0IsTUFBTTtBQUNSLENBQUM7QUF2QkcsTUFBQSxhQUFBLEVBQUEsT0FBTSxxRUFBb0U7QUFHeEUsTUFBQSxhQUFBQSxnQ0FBc0MsT0FBakMsRUFBQSxPQUFBLEVBQUEsYUFBQSxPQUFBLEtBQXdCLE9BQUcsRUFBQTttQkFDaENBLGdDQUFxRSxPQUFBO0FBQUEsRUFBaEUsT0FBTTtBQUFBLEVBQVUsT0FBQSxFQUFvQixXQUFBLE1BQUE7R0FBQyx5QkFBcUIsRUFBQTs7QUFMbkUsU0FBQUMsVUFBQSxHQUFBQyxtQkFnQk0sT0FoQk4sWUFnQk07QUFBQSxJQWJKRixnQkFZTSxPQUFBLE1BQUE7QUFBQSxNQVhKO0FBQUEsTUFDQTtBQUFBLE1BQ0FHLFlBUUUsTUFBQTtBQUFBLFFBUEEsT0FBTTtBQUFBLFFBQ04sT0FBTTtBQUFBLFFBQ04sY0FBVztBQUFBLFFBQ1gsWUFBQTtBQUFBLFFBQ0EsSUFBRztBQUFBLFFBQ0gsT0FBTTtBQUFBLFFBQ04sV0FBQTtBQUFBOzs7Ozs7In0=
