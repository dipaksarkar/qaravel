import {
  _ as _export_sfc,
  bC as mapActions,
  bv as mapState,
  bd as resolveComponent,
  X as createElementBlock,
  W as createBaseVNode,
  d as createVNode,
  V as withCtx,
  a3 as QCard,
  S as openBlock,
  Z as QCardSection,
  aK as QInput,
} from "./index.94c1c68b.js";
import { Q as QForm } from "./QForm.e37934b9.js";
import { u as useAppStore } from "./app.a880281b.js";
import "./api.636d00fa.js";
import "./BaseAlert.abd8bfd6.js";
import "./lodash.2b44b28e.js";
const _sfc_main = {
  data() {
    return {
      visible: false,
      errors: {},
    };
  },
  methods: {
    ...mapActions(useAppStore, ["update", "currentUser"]),
    onSaveProfile() {
      console.func("pages/SignUpPage:methods.onSaveProfile()", arguments);
      this.visible = true;
      this.errors = {};
      this.update({
        ...this.user,
        guard: this.$route.meta.guard,
      })
        .then(() => {
          this.visible = false;
        })
        .catch(({ errors, message }) => {
          this.visible = false;
          if (errors) {
            this.errors = errors;
          } else {
            this.$core.error(message);
          }
        });
    },
  },
  computed: {
    ...mapState(useAppStore, ["user"]),
  },
};
const _hoisted_1 = { class: "q-gutter-y-lg" };
const _hoisted_2 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-h6" },
  "Profile Information",
  -1
);
const _hoisted_3 = { class: "row q-pt-lg q-col-gutter-y-sm q-col-gutter-x-xl" };
const _hoisted_4 = { class: "col-sm-3 col-xs-12" };
const _hoisted_5 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "First Name",
  -1
);
const _hoisted_6 = { class: "col-sm-3 col-xs-12" };
const _hoisted_7 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "Surname",
  -1
);
const _hoisted_8 = { class: "col-sm-3 col-xs-12" };
const _hoisted_9 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "Email",
  -1
);
const _hoisted_10 = { class: "col-sm-3 col-xs-12" };
const _hoisted_11 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "Phone Number",
  -1
);
const _hoisted_12 = { class: "col-sm-3 col-xs-12" };
const _hoisted_13 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "Address Line 1",
  -1
);
const _hoisted_14 = { class: "col-sm-3 col-xs-12" };
const _hoisted_15 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "Address Line 2",
  -1
);
const _hoisted_16 = { class: "col-sm-3 col-xs-12" };
const _hoisted_17 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "City",
  -1
);
const _hoisted_18 = { class: "col-sm-3 col-xs-12" };
const _hoisted_19 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "Postcode/Zip",
  -1
);
const _hoisted_20 = { class: "col-sm-3 col-xs-12" };
const _hoisted_21 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "Country",
  -1
);
const _hoisted_22 = { class: "col-sm-3 col-xs-12" };
const _hoisted_23 = /* @__PURE__ */ createBaseVNode(
  "div",
  { class: "text-label" },
  "Password",
  -1
);
const _hoisted_24 = { class: "col-sm-6 col-xs-12 text-right self-end" };
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_base_btn = resolveComponent("base-btn");
  return (
    openBlock(),
    createElementBlock("div", null, [
      createBaseVNode("div", _hoisted_1, [
        createVNode(QCard, null, {
          default: withCtx(() => [
            createVNode(QCardSection, null, {
              default: withCtx(() => [
                _hoisted_2,
                createVNode(
                  QForm,
                  { onSubmit: $options.onSaveProfile },
                  {
                    default: withCtx(() => [
                      createBaseVNode("div", _hoisted_3, [
                        createBaseVNode("div", _hoisted_4, [
                          _hoisted_5,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "first_name",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError(
                                "first_name",
                                $data.errors
                              ),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.first_name,
                              "onUpdate:modelValue":
                                _cache[0] ||
                                (_cache[0] = ($event) =>
                                  (_ctx.user.first_name = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_6, [
                          _hoisted_7,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "last_name",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError(
                                "last_name",
                                $data.errors
                              ),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.last_name,
                              "onUpdate:modelValue":
                                _cache[1] ||
                                (_cache[1] = ($event) =>
                                  (_ctx.user.last_name = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_8, [
                          _hoisted_9,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "email",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError("email", $data.errors),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.email,
                              "onUpdate:modelValue":
                                _cache[2] ||
                                (_cache[2] = ($event) =>
                                  (_ctx.user.email = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_10, [
                          _hoisted_11,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "phone_number",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError(
                                "phone_number",
                                $data.errors
                              ),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.phone_number,
                              "onUpdate:modelValue":
                                _cache[3] ||
                                (_cache[3] = ($event) =>
                                  (_ctx.user.phone_number = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_12, [
                          _hoisted_13,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "line1",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError("line1", $data.errors),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.address.line1,
                              "onUpdate:modelValue":
                                _cache[4] ||
                                (_cache[4] = ($event) =>
                                  (_ctx.user.address.line1 = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_14, [
                          _hoisted_15,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "line2",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError("line2", $data.errors),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.address.line2,
                              "onUpdate:modelValue":
                                _cache[5] ||
                                (_cache[5] = ($event) =>
                                  (_ctx.user.address.line2 = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_16, [
                          _hoisted_17,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "city",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError("city", $data.errors),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.address.city,
                              "onUpdate:modelValue":
                                _cache[6] ||
                                (_cache[6] = ($event) =>
                                  (_ctx.user.address.city = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_18, [
                          _hoisted_19,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "postal_code",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError(
                                "postal_code",
                                $data.errors
                              ),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.address.postal_code,
                              "onUpdate:modelValue":
                                _cache[7] ||
                                (_cache[7] = ($event) =>
                                  (_ctx.user.address.postal_code = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_20, [
                          _hoisted_21,
                          createVNode(
                            QInput,
                            {
                              "error-message": _ctx.$core.errorMessage(
                                "country",
                                $data.errors
                              ),
                              error: _ctx.$core.hasError(
                                "country",
                                $data.errors
                              ),
                              dense: "",
                              outlined: "",
                              modelValue: _ctx.user.address.country,
                              "onUpdate:modelValue":
                                _cache[8] ||
                                (_cache[8] = ($event) =>
                                  (_ctx.user.address.country = $event)),
                              type: "text",
                            },
                            null,
                            8,
                            ["error-message", "error", "modelValue"]
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_22, [
                          _hoisted_23,
                          createVNode(
                            QInput,
                            {
                              type: "password",
                              readonly: "",
                              "bottom-slots": "",
                              dense: "",
                              outlined: "",
                              modelValue: "**********",
                            },
                            {
                              hint: withCtx(() => [
                                createVNode(_component_base_btn, {
                                  size: "11px",
                                  color: "grey",
                                  to: { name: "Forget Password" },
                                  link: "",
                                  type: "a",
                                  label: "Forgotten password?",
                                }),
                              ]),
                              _: 1,
                            }
                          ),
                        ]),
                        createBaseVNode("div", _hoisted_24, [
                          createVNode(
                            _component_base_btn,
                            {
                              class: "main-btn",
                              loading: $data.visible,
                              style: { width: "100px" },
                              label: "Save",
                              type: "submit",
                            },
                            null,
                            8,
                            ["loading"]
                          ),
                        ]),
                      ]),
                    ]),
                    _: 1,
                  },
                  8,
                  ["onSubmit"]
                ),
              ]),
              _: 1,
            }),
          ]),
          _: 1,
        }),
      ]),
    ])
  );
}
var MyAccountPage = /* @__PURE__ */ _export_sfc(_sfc_main, [
  ["render", _sfc_render],
  ["__file", "MyAccountPage.vue"],
]);
export { MyAccountPage as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTXlBY2NvdW50UGFnZS4wZmNjMWFhYS5qcyIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL3BhZ2VzL015QWNjb3VudFBhZ2UudnVlIl0sInNvdXJjZXNDb250ZW50IjpbIjx0ZW1wbGF0ZT5cbiAgPGRpdj5cbiAgICA8ZGl2IGNsYXNzPVwicS1ndXR0ZXIteS1sZ1wiPlxuICAgICAgPHEtY2FyZD5cbiAgICAgICAgPHEtY2FyZC1zZWN0aW9uPlxuICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LWg2XCI+UHJvZmlsZSBJbmZvcm1hdGlvbjwvZGl2PlxuXG4gICAgICAgICAgPHEtZm9ybSBAc3VibWl0PVwib25TYXZlUHJvZmlsZVwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBxLXB0LWxnIHEtY29sLWd1dHRlci15LXNtIHEtY29sLWd1dHRlci14LXhsXCI+XG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMyBjb2wteHMtMTJcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1sYWJlbFwiPkZpcnN0IE5hbWU8L2Rpdj5cblxuICAgICAgICAgICAgICAgIDxxLWlucHV0XG4gICAgICAgICAgICAgICAgICA6ZXJyb3ItbWVzc2FnZT1cIiRjb3JlLmVycm9yTWVzc2FnZSgnZmlyc3RfbmFtZScsIGVycm9ycylcIlxuICAgICAgICAgICAgICAgICAgOmVycm9yPVwiJGNvcmUuaGFzRXJyb3IoJ2ZpcnN0X25hbWUnLCBlcnJvcnMpXCJcbiAgICAgICAgICAgICAgICAgIGRlbnNlXG4gICAgICAgICAgICAgICAgICBvdXRsaW5lZFxuICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cInVzZXIuZmlyc3RfbmFtZVwiXG4gICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0zIGNvbC14cy0xMlwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LWxhYmVsXCI+U3VybmFtZTwvZGl2PlxuXG4gICAgICAgICAgICAgICAgPHEtaW5wdXRcbiAgICAgICAgICAgICAgICAgIDplcnJvci1tZXNzYWdlPVwiJGNvcmUuZXJyb3JNZXNzYWdlKCdzdXJuYW1lJywgZXJyb3JzKVwiXG4gICAgICAgICAgICAgICAgICA6ZXJyb3I9XCIkY29yZS5oYXNFcnJvcignc3VybmFtZScsIGVycm9ycylcIlxuICAgICAgICAgICAgICAgICAgZGVuc2VcbiAgICAgICAgICAgICAgICAgIG91dGxpbmVkXG4gICAgICAgICAgICAgICAgICB2LW1vZGVsPVwidXNlci5zdXJuYW1lXCJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTMgY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtbGFiZWxcIj5FbWFpbDwvZGl2PlxuXG4gICAgICAgICAgICAgICAgPHEtaW5wdXRcbiAgICAgICAgICAgICAgICAgIDplcnJvci1tZXNzYWdlPVwiJGNvcmUuZXJyb3JNZXNzYWdlKCdlbWFpbCcsIGVycm9ycylcIlxuICAgICAgICAgICAgICAgICAgOmVycm9yPVwiJGNvcmUuaGFzRXJyb3IoJ2VtYWlsJywgZXJyb3JzKVwiXG4gICAgICAgICAgICAgICAgICBkZW5zZVxuICAgICAgICAgICAgICAgICAgb3V0bGluZWRcbiAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJ1c2VyLmVtYWlsXCJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTMgY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtbGFiZWxcIj5QaG9uZSBOdW1iZXI8L2Rpdj5cblxuICAgICAgICAgICAgICAgIDxxLWlucHV0XG4gICAgICAgICAgICAgICAgICA6ZXJyb3ItbWVzc2FnZT1cIiRjb3JlLmVycm9yTWVzc2FnZSgncGhvbmVfbnVtYmVyJywgZXJyb3JzKVwiXG4gICAgICAgICAgICAgICAgICA6ZXJyb3I9XCIkY29yZS5oYXNFcnJvcigncGhvbmVfbnVtYmVyJywgZXJyb3JzKVwiXG4gICAgICAgICAgICAgICAgICBkZW5zZVxuICAgICAgICAgICAgICAgICAgb3V0bGluZWRcbiAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJ1c2VyLnBob25lX251bWJlclwiXG4gICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0zIGNvbC14cy0xMlwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LWxhYmVsXCI+QWRkcmVzcyBMaW5lIDE8L2Rpdj5cblxuICAgICAgICAgICAgICAgIDxxLWlucHV0XG4gICAgICAgICAgICAgICAgICA6ZXJyb3ItbWVzc2FnZT1cIiRjb3JlLmVycm9yTWVzc2FnZSgnbGluZTEnLCBlcnJvcnMpXCJcbiAgICAgICAgICAgICAgICAgIDplcnJvcj1cIiRjb3JlLmhhc0Vycm9yKCdsaW5lMScsIGVycm9ycylcIlxuICAgICAgICAgICAgICAgICAgZGVuc2VcbiAgICAgICAgICAgICAgICAgIG91dGxpbmVkXG4gICAgICAgICAgICAgICAgICB2LW1vZGVsPVwidXNlci5hZGRyZXNzLmxpbmUxXCJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTMgY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtbGFiZWxcIj5BZGRyZXNzIExpbmUgMjwvZGl2PlxuXG4gICAgICAgICAgICAgICAgPHEtaW5wdXRcbiAgICAgICAgICAgICAgICAgIDplcnJvci1tZXNzYWdlPVwiJGNvcmUuZXJyb3JNZXNzYWdlKCdsaW5lMicsIGVycm9ycylcIlxuICAgICAgICAgICAgICAgICAgOmVycm9yPVwiJGNvcmUuaGFzRXJyb3IoJ2xpbmUyJywgZXJyb3JzKVwiXG4gICAgICAgICAgICAgICAgICBkZW5zZVxuICAgICAgICAgICAgICAgICAgb3V0bGluZWRcbiAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJ1c2VyLmFkZHJlc3MubGluZTJcIlxuICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJjb2wtc20tMyBjb2wteHMtMTJcIj5cbiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwidGV4dC1sYWJlbFwiPkNpdHk8L2Rpdj5cblxuICAgICAgICAgICAgICAgIDxxLWlucHV0XG4gICAgICAgICAgICAgICAgICA6ZXJyb3ItbWVzc2FnZT1cIiRjb3JlLmVycm9yTWVzc2FnZSgnY2l0eScsIGVycm9ycylcIlxuICAgICAgICAgICAgICAgICAgOmVycm9yPVwiJGNvcmUuaGFzRXJyb3IoJ2NpdHknLCBlcnJvcnMpXCJcbiAgICAgICAgICAgICAgICAgIGRlbnNlXG4gICAgICAgICAgICAgICAgICBvdXRsaW5lZFxuICAgICAgICAgICAgICAgICAgdi1tb2RlbD1cInVzZXIuYWRkcmVzcy5jaXR5XCJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTMgY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtbGFiZWxcIj5Qb3N0Y29kZS9aaXA8L2Rpdj5cblxuICAgICAgICAgICAgICAgIDxxLWlucHV0XG4gICAgICAgICAgICAgICAgICA6ZXJyb3ItbWVzc2FnZT1cIiRjb3JlLmVycm9yTWVzc2FnZSgncG9zdGFsX2NvZGUnLCBlcnJvcnMpXCJcbiAgICAgICAgICAgICAgICAgIDplcnJvcj1cIiRjb3JlLmhhc0Vycm9yKCdwb3N0YWxfY29kZScsIGVycm9ycylcIlxuICAgICAgICAgICAgICAgICAgZGVuc2VcbiAgICAgICAgICAgICAgICAgIG91dGxpbmVkXG4gICAgICAgICAgICAgICAgICB2LW1vZGVsPVwidXNlci5hZGRyZXNzLnBvc3RhbF9jb2RlXCJcbiAgICAgICAgICAgICAgICAgIHR5cGU9XCJ0ZXh0XCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTMgY29sLXhzLTEyXCI+XG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cInRleHQtbGFiZWxcIj5Db3VudHJ5PC9kaXY+XG5cbiAgICAgICAgICAgICAgICA8cS1pbnB1dFxuICAgICAgICAgICAgICAgICAgOmVycm9yLW1lc3NhZ2U9XCIkY29yZS5lcnJvck1lc3NhZ2UoJ2NvdW50cnknLCBlcnJvcnMpXCJcbiAgICAgICAgICAgICAgICAgIDplcnJvcj1cIiRjb3JlLmhhc0Vycm9yKCdjb3VudHJ5JywgZXJyb3JzKVwiXG4gICAgICAgICAgICAgICAgICBkZW5zZVxuICAgICAgICAgICAgICAgICAgb3V0bGluZWRcbiAgICAgICAgICAgICAgICAgIHYtbW9kZWw9XCJ1c2VyLmFkZHJlc3MuY291bnRyeVwiXG4gICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICAgPGRpdiBjbGFzcz1cImNvbC1zbS0zIGNvbC14cy0xMlwiPlxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJ0ZXh0LWxhYmVsXCI+UGFzc3dvcmQ8L2Rpdj5cblxuICAgICAgICAgICAgICAgIDxxLWlucHV0XG4gICAgICAgICAgICAgICAgICB0eXBlPVwicGFzc3dvcmRcIlxuICAgICAgICAgICAgICAgICAgcmVhZG9ubHlcbiAgICAgICAgICAgICAgICAgIGJvdHRvbS1zbG90c1xuICAgICAgICAgICAgICAgICAgZGVuc2VcbiAgICAgICAgICAgICAgICAgIG91dGxpbmVkXG4gICAgICAgICAgICAgICAgICBtb2RlbFZhbHVlPVwiKioqKioqKioqKlwiXG4gICAgICAgICAgICAgICAgPlxuICAgICAgICAgICAgICAgICAgPHRlbXBsYXRlIHYtc2xvdDpoaW50PlxuICAgICAgICAgICAgICAgICAgICA8YmFzZS1idG5cbiAgICAgICAgICAgICAgICAgICAgICBzaXplPVwiMTFweFwiXG4gICAgICAgICAgICAgICAgICAgICAgY29sb3I9XCJncmV5XCJcbiAgICAgICAgICAgICAgICAgICAgICA6dG89XCJ7IG5hbWU6ICdGb3JnZXQgUGFzc3dvcmQnIH1cIlxuICAgICAgICAgICAgICAgICAgICAgIGxpbmtcbiAgICAgICAgICAgICAgICAgICAgICB0eXBlPVwiYVwiXG4gICAgICAgICAgICAgICAgICAgICAgbGFiZWw9XCJGb3Jnb3R0ZW4gcGFzc3dvcmQ/XCJcbiAgICAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgIDwvdGVtcGxhdGU+XG4gICAgICAgICAgICAgICAgPC9xLWlucHV0PlxuICAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwiY29sLXNtLTYgY29sLXhzLTEyIHRleHQtcmlnaHQgc2VsZi1lbmRcIj5cbiAgICAgICAgICAgICAgICA8YmFzZS1idG5cbiAgICAgICAgICAgICAgICAgIGNsYXNzPVwibWFpbi1idG5cIlxuICAgICAgICAgICAgICAgICAgOmxvYWRpbmc9XCJ2aXNpYmxlXCJcbiAgICAgICAgICAgICAgICAgIHN0eWxlPVwid2lkdGg6IDEwMHB4XCJcbiAgICAgICAgICAgICAgICAgIGxhYmVsPVwiU2F2ZVwiXG4gICAgICAgICAgICAgICAgICB0eXBlPVwic3VibWl0XCJcbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvcS1mb3JtPlxuICAgICAgICA8L3EtY2FyZC1zZWN0aW9uPlxuICAgICAgPC9xLWNhcmQ+XG4gICAgPC9kaXY+XG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmltcG9ydCB7IG1hcEFjdGlvbnMsIG1hcFN0YXRlIH0gZnJvbSBcInBpbmlhXCI7XG5pbXBvcnQgeyB1c2VBcHBTdG9yZSB9IGZyb20gXCJzdG9yZXMvYXBwXCI7XG5jb25zdCBndWFyZCA9IFwidXNlcnNcIjtcblxuZXhwb3J0IGRlZmF1bHQge1xuICBkYXRhKCkge1xuICAgIHJldHVybiB7XG4gICAgICB2aXNpYmxlOiBmYWxzZSxcbiAgICAgIGVycm9yczoge30sXG4gICAgfTtcbiAgfSxcbiAgbWV0aG9kczoge1xuICAgIC4uLm1hcEFjdGlvbnModXNlQXBwU3RvcmUsIFtcInVwZGF0ZVwiLCBcImN1cnJlbnRVc2VyXCJdKSxcbiAgICBvblNhdmVQcm9maWxlKCkge1xuICAgICAgY29uc29sZS5mdW5jKFwicGFnZXMvU2lnblVwUGFnZTptZXRob2RzLm9uU2F2ZVByb2ZpbGUoKVwiLCBhcmd1bWVudHMpO1xuICAgICAgdGhpcy52aXNpYmxlID0gdHJ1ZTtcbiAgICAgIHRoaXMuZXJyb3JzID0ge307XG4gICAgICB0aGlzLnVwZGF0ZSh7XG4gICAgICAgIC4uLnRoaXMudXNlcixcbiAgICAgICAgZ3VhcmQ6IHRoaXMuJHJvdXRlLm1ldGEuZ3VhcmQsXG4gICAgICB9KVxuICAgICAgICAudGhlbigoKSA9PiB7XG4gICAgICAgICAgdGhpcy52aXNpYmxlID0gZmFsc2U7XG4gICAgICAgIH0pXG4gICAgICAgIC5jYXRjaCgoeyBlcnJvcnMsIG1lc3NhZ2UgfSkgPT4ge1xuICAgICAgICAgIHRoaXMudmlzaWJsZSA9IGZhbHNlO1xuICAgICAgICAgIGlmIChlcnJvcnMpIHtcbiAgICAgICAgICAgIHRoaXMuZXJyb3JzID0gZXJyb3JzO1xuICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLiRjb3JlLmVycm9yKG1lc3NhZ2UpO1xuICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfSxcbiAgfSxcbiAgY29tcHV0ZWQ6IHtcbiAgICAuLi5tYXBTdGF0ZSh1c2VBcHBTdG9yZSwgW1widXNlclwiXSksXG4gIH0sXG59O1xuPC9zY3JpcHQ+XG4iXSwibmFtZXMiOlsiX2NyZWF0ZUVsZW1lbnRWTm9kZSIsIl9jcmVhdGVFbGVtZW50QmxvY2siLCJfY3JlYXRlVk5vZGUiXSwibWFwcGluZ3MiOiI7Ozs7OztBQTRLQSxNQUFLLFlBQVU7QUFBQSxFQUNiLE9BQU87QUFDTCxXQUFPO0FBQUEsTUFDTCxTQUFTO0FBQUEsTUFDVCxRQUFRLENBQUU7QUFBQTtFQUViO0FBQUEsRUFDRCxTQUFTO0FBQUEsSUFDUCxHQUFHLFdBQVcsYUFBYSxDQUFDLFVBQVUsYUFBYSxDQUFDO0FBQUEsSUFDcEQsZ0JBQWdCO0FBQ2QsY0FBUSxLQUFLLDRDQUE0QyxTQUFTO0FBQ2xFLFdBQUssVUFBVTtBQUNmLFdBQUssU0FBUztBQUNkLFdBQUssT0FBTztBQUFBLFFBQ1YsR0FBRyxLQUFLO0FBQUEsUUFDUixPQUFPLEtBQUssT0FBTyxLQUFLO0FBQUEsT0FDekIsRUFDRSxLQUFLLE1BQU07QUFDVixhQUFLLFVBQVU7QUFBQSxPQUNoQixFQUNBLE1BQU0sQ0FBQyxFQUFFLFFBQVEsY0FBYztBQUM5QixhQUFLLFVBQVU7QUFDZixZQUFJLFFBQVE7QUFDVixlQUFLLFNBQVM7QUFBQSxlQUNUO0FBQ0wsZUFBSyxNQUFNLE1BQU0sT0FBTztBQUFBLFFBQzFCO0FBQUEsTUFDRixDQUFDO0FBQUEsSUFDSjtBQUFBLEVBQ0Y7QUFBQSxFQUNELFVBQVU7QUFBQSxJQUNSLEdBQUcsU0FBUyxhQUFhLENBQUMsTUFBTSxDQUFDO0FBQUEsRUFDbEM7QUFDSDtBQTNNUyxNQUFBLGFBQUEsRUFBQSxPQUFNLGdCQUFlO0FBR3BCLE1BQUEsYUFBQUEsZ0NBQThDLE9BQXpDLEVBQUEsT0FBTSxhQUFVLHVCQUFtQixFQUFBO0FBR2pDLE1BQUEsYUFBQSxFQUFBLE9BQU0sa0RBQWlEO0FBQ3JELE1BQUEsYUFBQSxFQUFBLE9BQU0scUJBQW9CO0FBQzdCLE1BQUEsYUFBQUEsZ0NBQXdDLE9BQW5DLEVBQUEsT0FBTSxnQkFBYSxjQUFVLEVBQUE7QUFZL0IsTUFBQSxhQUFBLEVBQUEsT0FBTSxxQkFBb0I7QUFDN0IsTUFBQSxhQUFBQSxnQ0FBcUMsT0FBaEMsRUFBQSxPQUFNLGdCQUFhLFdBQU8sRUFBQTtBQVk1QixNQUFBLGFBQUEsRUFBQSxPQUFNLHFCQUFvQjtBQUM3QixNQUFBLGFBQUFBLGdDQUFtQyxPQUE5QixFQUFBLE9BQU0sZ0JBQWEsU0FBSyxFQUFBO0FBWTFCLE1BQUEsY0FBQSxFQUFBLE9BQU0scUJBQW9CO0FBQzdCLE1BQUEsY0FBQUEsZ0NBQTBDLE9BQXJDLEVBQUEsT0FBTSxnQkFBYSxnQkFBWSxFQUFBO0FBWWpDLE1BQUEsY0FBQSxFQUFBLE9BQU0scUJBQW9CO0FBQzdCLE1BQUEsY0FBQUEsZ0NBQTRDLE9BQXZDLEVBQUEsT0FBTSxnQkFBYSxrQkFBYyxFQUFBO0FBWW5DLE1BQUEsY0FBQSxFQUFBLE9BQU0scUJBQW9CO0FBQzdCLE1BQUEsY0FBQUEsZ0NBQTRDLE9BQXZDLEVBQUEsT0FBTSxnQkFBYSxrQkFBYyxFQUFBO0FBWW5DLE1BQUEsY0FBQSxFQUFBLE9BQU0scUJBQW9CO0FBQzdCLE1BQUEsY0FBQUEsZ0NBQWtDLE9BQTdCLEVBQUEsT0FBTSxnQkFBYSxRQUFJLEVBQUE7QUFZekIsTUFBQSxjQUFBLEVBQUEsT0FBTSxxQkFBb0I7QUFDN0IsTUFBQSxjQUFBQSxnQ0FBMEMsT0FBckMsRUFBQSxPQUFNLGdCQUFhLGdCQUFZLEVBQUE7QUFZakMsTUFBQSxjQUFBLEVBQUEsT0FBTSxxQkFBb0I7QUFDN0IsTUFBQSxjQUFBQSxnQ0FBcUMsT0FBaEMsRUFBQSxPQUFNLGdCQUFhLFdBQU8sRUFBQTtBQVk1QixNQUFBLGNBQUEsRUFBQSxPQUFNLHFCQUFvQjtBQUM3QixNQUFBLGNBQUFBLGdDQUFzQyxPQUFqQyxFQUFBLE9BQU0sZ0JBQWEsWUFBUSxFQUFBO0FBdUI3QixNQUFBLGNBQUEsRUFBQSxPQUFNLHlDQUF3Qzs7O3NCQXJKL0RDLG1CQW1LTSxPQUFBLE1BQUE7QUFBQSxJQWxLSkQsZ0JBaUtNLE9BaktOLFlBaUtNO0FBQUEsTUFoS0pFLFlBK0pTLE9BQUEsTUFBQTtBQUFBLHlCQTlKUCxNQTZKaUI7QUFBQSxVQTdKakJBLFlBNkppQixjQUFBLE1BQUE7QUFBQSw2QkE1SmYsTUFBOEM7QUFBQSxjQUE5QztBQUFBLGNBRUFBLFlBeUpTLE9BQUEsRUFBQSxVQUFBLFNBekpNLGNBQWUsR0FBQTtBQUFBLGlDQUM1QixNQXVKTTtBQUFBLGtCQXZKTkYsZ0JBdUpNLE9BdkpOLFlBdUpNO0FBQUEsb0JBdEpKQSxnQkFXTSxPQVhOLFlBV007QUFBQSxzQkFWSjtBQUFBLHNCQUVBRSxZQU9FLFFBQUE7QUFBQSx3QkFOQyxpQkFBZSxLQUFBLE1BQU0sMkJBQTJCLE1BQU0sTUFBQTtBQUFBLHdCQUN0RCxPQUFPLEtBQUEsTUFBTSx1QkFBdUIsTUFBTSxNQUFBO0FBQUEsd0JBQzNDLE9BQUE7QUFBQSx3QkFDQSxVQUFBO0FBQUEsd0JBQ1MsWUFBQSxLQUFBLEtBQUs7QUFBQSx3QkFBTCx1QkFBQSxPQUFBLE9BQUEsT0FBQSxLQUFBLFlBQUEsS0FBQSxLQUFLLGFBQVU7QUFBQSx3QkFDeEIsTUFBSztBQUFBOztvQkFJVEYsZ0JBV00sT0FYTixZQVdNO0FBQUEsc0JBVko7QUFBQSxzQkFFQUUsWUFPRSxRQUFBO0FBQUEsd0JBTkMsaUJBQWUsS0FBQSxNQUFNLHdCQUF3QixNQUFNLE1BQUE7QUFBQSx3QkFDbkQsT0FBTyxLQUFBLE1BQU0sb0JBQW9CLE1BQU0sTUFBQTtBQUFBLHdCQUN4QyxPQUFBO0FBQUEsd0JBQ0EsVUFBQTtBQUFBLHdCQUNTLFlBQUEsS0FBQSxLQUFLO0FBQUEsd0JBQUwsdUJBQUEsT0FBQSxPQUFBLE9BQUEsS0FBQSxZQUFBLEtBQUEsS0FBSyxVQUFPO0FBQUEsd0JBQ3JCLE1BQUs7QUFBQTs7b0JBSVRGLGdCQVdNLE9BWE4sWUFXTTtBQUFBLHNCQVZKO0FBQUEsc0JBRUFFLFlBT0UsUUFBQTtBQUFBLHdCQU5DLGlCQUFlLEtBQUEsTUFBTSxzQkFBc0IsTUFBTSxNQUFBO0FBQUEsd0JBQ2pELE9BQU8sS0FBQSxNQUFNLGtCQUFrQixNQUFNLE1BQUE7QUFBQSx3QkFDdEMsT0FBQTtBQUFBLHdCQUNBLFVBQUE7QUFBQSx3QkFDUyxZQUFBLEtBQUEsS0FBSztBQUFBLHdCQUFMLHVCQUFBLE9BQUEsT0FBQSxPQUFBLEtBQUEsWUFBQSxLQUFBLEtBQUssUUFBSztBQUFBLHdCQUNuQixNQUFLO0FBQUE7O29CQUlURixnQkFXTSxPQVhOLGFBV007QUFBQSxzQkFWSjtBQUFBLHNCQUVBRSxZQU9FLFFBQUE7QUFBQSx3QkFOQyxpQkFBZSxLQUFBLE1BQU0sNkJBQTZCLE1BQU0sTUFBQTtBQUFBLHdCQUN4RCxPQUFPLEtBQUEsTUFBTSx5QkFBeUIsTUFBTSxNQUFBO0FBQUEsd0JBQzdDLE9BQUE7QUFBQSx3QkFDQSxVQUFBO0FBQUEsd0JBQ1MsWUFBQSxLQUFBLEtBQUs7QUFBQSx3QkFBTCx1QkFBQSxPQUFBLE9BQUEsT0FBQSxLQUFBLFlBQUEsS0FBQSxLQUFLLGVBQVk7QUFBQSx3QkFDMUIsTUFBSztBQUFBOztvQkFJVEYsZ0JBV00sT0FYTixhQVdNO0FBQUEsc0JBVko7QUFBQSxzQkFFQUUsWUFPRSxRQUFBO0FBQUEsd0JBTkMsaUJBQWUsS0FBQSxNQUFNLHNCQUFzQixNQUFNLE1BQUE7QUFBQSx3QkFDakQsT0FBTyxLQUFBLE1BQU0sa0JBQWtCLE1BQU0sTUFBQTtBQUFBLHdCQUN0QyxPQUFBO0FBQUEsd0JBQ0EsVUFBQTtBQUFBLG9DQUNTLEtBQUksS0FBQyxRQUFRO0FBQUEscUZBQWIsS0FBSSxLQUFDLFFBQVEsUUFBSztBQUFBLHdCQUMzQixNQUFLO0FBQUE7O29CQUlURixnQkFXTSxPQVhOLGFBV007QUFBQSxzQkFWSjtBQUFBLHNCQUVBRSxZQU9FLFFBQUE7QUFBQSx3QkFOQyxpQkFBZSxLQUFBLE1BQU0sc0JBQXNCLE1BQU0sTUFBQTtBQUFBLHdCQUNqRCxPQUFPLEtBQUEsTUFBTSxrQkFBa0IsTUFBTSxNQUFBO0FBQUEsd0JBQ3RDLE9BQUE7QUFBQSx3QkFDQSxVQUFBO0FBQUEsb0NBQ1MsS0FBSSxLQUFDLFFBQVE7QUFBQSxxRkFBYixLQUFJLEtBQUMsUUFBUSxRQUFLO0FBQUEsd0JBQzNCLE1BQUs7QUFBQTs7b0JBSVRGLGdCQVdNLE9BWE4sYUFXTTtBQUFBLHNCQVZKO0FBQUEsc0JBRUFFLFlBT0UsUUFBQTtBQUFBLHdCQU5DLGlCQUFlLEtBQUEsTUFBTSxxQkFBcUIsTUFBTSxNQUFBO0FBQUEsd0JBQ2hELE9BQU8sS0FBQSxNQUFNLGlCQUFpQixNQUFNLE1BQUE7QUFBQSx3QkFDckMsT0FBQTtBQUFBLHdCQUNBLFVBQUE7QUFBQSxvQ0FDUyxLQUFJLEtBQUMsUUFBUTtBQUFBLHFGQUFiLEtBQUksS0FBQyxRQUFRLE9BQUk7QUFBQSx3QkFDMUIsTUFBSztBQUFBOztvQkFJVEYsZ0JBV00sT0FYTixhQVdNO0FBQUEsc0JBVko7QUFBQSxzQkFFQUUsWUFPRSxRQUFBO0FBQUEsd0JBTkMsaUJBQWUsS0FBQSxNQUFNLDRCQUE0QixNQUFNLE1BQUE7QUFBQSx3QkFDdkQsT0FBTyxLQUFBLE1BQU0sd0JBQXdCLE1BQU0sTUFBQTtBQUFBLHdCQUM1QyxPQUFBO0FBQUEsd0JBQ0EsVUFBQTtBQUFBLG9DQUNTLEtBQUksS0FBQyxRQUFRO0FBQUEscUZBQWIsS0FBSSxLQUFDLFFBQVEsY0FBVztBQUFBLHdCQUNqQyxNQUFLO0FBQUE7O29CQUlURixnQkFXTSxPQVhOLGFBV007QUFBQSxzQkFWSjtBQUFBLHNCQUVBRSxZQU9FLFFBQUE7QUFBQSx3QkFOQyxpQkFBZSxLQUFBLE1BQU0sd0JBQXdCLE1BQU0sTUFBQTtBQUFBLHdCQUNuRCxPQUFPLEtBQUEsTUFBTSxvQkFBb0IsTUFBTSxNQUFBO0FBQUEsd0JBQ3hDLE9BQUE7QUFBQSx3QkFDQSxVQUFBO0FBQUEsb0NBQ1MsS0FBSSxLQUFDLFFBQVE7QUFBQSxxRkFBYixLQUFJLEtBQUMsUUFBUSxVQUFPO0FBQUEsd0JBQzdCLE1BQUs7QUFBQTs7b0JBSVRGLGdCQXNCTSxPQXRCTixhQXNCTTtBQUFBLHNCQXJCSjtBQUFBLHNCQUVBRSxZQWtCVSxRQUFBO0FBQUEsd0JBakJSLE1BQUs7QUFBQSx3QkFDTCxVQUFBO0FBQUEsd0JBQ0EsZ0JBQUE7QUFBQSx3QkFDQSxPQUFBO0FBQUEsd0JBQ0EsVUFBQTtBQUFBLHdCQUNBLFlBQVc7QUFBQTt3QkFFTSxjQUNmLE1BT0U7QUFBQSwwQkFQRkEsWUFPRSxxQkFBQTtBQUFBLDRCQU5BLE1BQUs7QUFBQSw0QkFDTCxPQUFNO0FBQUEsNEJBQ0wsSUFBSSxFQUEyQixNQUFBLGtCQUFBO0FBQUEsNEJBQ2hDLE1BQUE7QUFBQSw0QkFDQSxNQUFLO0FBQUEsNEJBQ0wsT0FBTTtBQUFBOzs7OztvQkFNZEYsZ0JBUU0sT0FSTixhQVFNO0FBQUEsc0JBUEpFLFlBTUUscUJBQUE7QUFBQSx3QkFMQSxPQUFNO0FBQUEsd0JBQ0wsU0FBUyxNQUFPO0FBQUEsd0JBQ2pCLE9BQUEsRUFBb0IsU0FBQSxRQUFBO0FBQUEsd0JBQ3BCLE9BQU07QUFBQSx3QkFDTixNQUFLO0FBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7OyJ9
