import { Q as QPage } from "./QPage.d14d1fc7.js";
import {
  Q as QLayout,
  _ as _imports_0,
  a as QPageContainer,
} from "./logo.8dc3ac27.js";
import {
  _ as _export_sfc,
  bd as resolveComponent,
  U as createBlock,
  V as withCtx,
  S as openBlock,
  d as createVNode,
  W as createBaseVNode,
} from "./index.94c1c68b.js";
var AuthLayout_vue_vue_type_style_index_0_lang = "";
const _sfc_main = {
  computed: {
    app_name: {
      get() {
        return "Qaravel";
      },
      set(val) {
        return val;
      },
    },
  },
};
const _hoisted_1 = { class: "login-container row vertical-middle" };
const _hoisted_2 = { class: "login-left col q-pa-xl" };
const _hoisted_3 = { class: "row items-center flex-center" };
const _hoisted_4 = ["alt"];
function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
  const _component_router_view = resolveComponent("router-view");
  return (
    openBlock(),
    createBlock(
      QLayout,
      {
        view: "hHh Lpr fFf",
        class: "bg-main",
      },
      {
        default: withCtx(() => [
          createVNode(QPageContainer, null, {
            default: withCtx(() => [
              createVNode(
                QPage,
                {
                  class: "row flex-center items-center vertical-middle",
                  padding: "",
                },
                {
                  default: withCtx(() => [
                    createBaseVNode("div", _hoisted_1, [
                      createBaseVNode("div", _hoisted_2, [
                        createBaseVNode("div", _hoisted_3, [
                          createBaseVNode(
                            "img",
                            {
                              style: { width: "150px" },
                              src: _imports_0,
                              transition: "fade",
                              alt: $options.app_name,
                              class: "gm-logo",
                            },
                            null,
                            8,
                            _hoisted_4
                          ),
                        ]),
                        createVNode(_component_router_view),
                      ]),
                    ]),
                  ]),
                  _: 1,
                }
              ),
            ]),
            _: 1,
          }),
        ]),
        _: 1,
      }
    )
  );
}
var AuthLayout = /* @__PURE__ */ _export_sfc(_sfc_main, [
  ["render", _sfc_render],
  ["__file", "AuthLayout.vue"],
]);
export { AuthLayout as default };
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiQXV0aExheW91dC5mNDNiM2M5Yi5qcyIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xheW91dHMvQXV0aExheW91dC52dWUiXSwic291cmNlc0NvbnRlbnQiOlsiPHRlbXBsYXRlPlxuICA8cS1sYXlvdXQgdmlldz1cImhIaCBMcHIgZkZmXCIgY2xhc3M9XCJiZy1tYWluXCI+XG4gICAgPHEtcGFnZS1jb250YWluZXI+XG4gICAgICA8cS1wYWdlIGNsYXNzPVwicm93IGZsZXgtY2VudGVyIGl0ZW1zLWNlbnRlciB2ZXJ0aWNhbC1taWRkbGVcIiBwYWRkaW5nPlxuICAgICAgICA8ZGl2IGNsYXNzPVwibG9naW4tY29udGFpbmVyIHJvdyB2ZXJ0aWNhbC1taWRkbGVcIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzPVwibG9naW4tbGVmdCBjb2wgcS1wYS14bFwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzcz1cInJvdyBpdGVtcy1jZW50ZXIgZmxleC1jZW50ZXJcIj5cbiAgICAgICAgICAgICAgPGltZyBzdHlsZT1cIndpZHRoOiAxNTBweFwiIHNyYz1cIn5hc3NldHMvbG9nby5wbmdcIiB0cmFuc2l0aW9uPVwiZmFkZVwiIDphbHQ9XCJhcHBfbmFtZVwiIGNsYXNzPVwiZ20tbG9nb1wiIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDxyb3V0ZXItdmlldyAvPlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIDwvcS1wYWdlPlxuICAgIDwvcS1wYWdlLWNvbnRhaW5lcj5cbiAgPC9xLWxheW91dD5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIGNvbXB1dGVkOiB7XG4gICAgYXBwX25hbWU6IHtcbiAgICAgIGdldCgpIHtcbiAgICAgICAgcmV0dXJuIHByb2Nlc3MuZW52LkFQUF9OQU1FO1xuICAgICAgfSxcbiAgICAgIHNldCh2YWwpIHtcbiAgICAgICAgcmV0dXJuIHZhbDtcbiAgICAgIH0sXG4gICAgfSxcbiAgfSxcbn07XG48L3NjcmlwdD5cblxuPHN0eWxlIGxhbmc9XCJzY3NzXCI+XG4ubG9naW4tY29udGFpbmVyIHtcbiAgd2lkdGg6IDQwMHB4O1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmO1xuICBib3gtc2hhZG93OiAwcHggOHB4IDI0cHggcmdiYSgxNTMsIDE1MywgMTUzLCAwLjE1KTtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbn1cbjwvc3R5bGU+XG4iXSwibmFtZXMiOlsiX2NyZWF0ZUJsb2NrIiwiX2NyZWF0ZVZOb2RlIiwiX2NyZWF0ZUVsZW1lbnRWTm9kZSJdLCJtYXBwaW5ncyI6Ijs7OztBQWtCQSxNQUFLLFlBQVU7QUFBQSxFQUNiLFVBQVU7QUFBQSxJQUNSLFVBQVU7QUFBQSxNQUNSLE1BQU07QUFDSixlQUFPO0FBQUEsTUFDUjtBQUFBLE1BQ0QsSUFBSSxLQUFLO0FBQ1AsZUFBTztBQUFBLE1BQ1I7QUFBQSxJQUNGO0FBQUEsRUFDRjtBQUNIO0FBekJhLE1BQUEsYUFBQSxFQUFBLE9BQU0sc0NBQXFDO0FBQ3pDLE1BQUEsYUFBQSxFQUFBLE9BQU0seUJBQXdCO0FBQzVCLE1BQUEsYUFBQSxFQUFBLE9BQU0sK0JBQThCOzs7O3NCQUxuREEsWUFhVyxTQUFBO0FBQUEsSUFiRCxNQUFLO0FBQUEsSUFBYyxPQUFNO0FBQUE7cUJBQ2pDLE1BV21CO0FBQUEsTUFYbkJDLFlBV21CLGdCQUFBLE1BQUE7QUFBQSx5QkFWakIsTUFTUztBQUFBLFVBVFRBLFlBU1MsT0FBQTtBQUFBLFlBVEQsT0FBTTtBQUFBLFlBQStDLFNBQUE7QUFBQTs2QkFDM0QsTUFPTTtBQUFBLGNBUE5DLGdCQU9NLE9BUE4sWUFPTTtBQUFBLGdCQU5KQSxnQkFLTSxPQUxOLFlBS007QUFBQSxrQkFKSkEsZ0JBRU0sT0FGTixZQUVNO0FBQUEsb0JBREpBLGdCQUFxRyxPQUFBO0FBQUEsc0JBQWhHLE9BQUEsRUFBb0IsU0FBQSxRQUFBO0FBQUEsc0JBQUMsS0FBQTtBQUFBLHNCQUF1QixZQUFXO0FBQUEsc0JBQVEsS0FBSyxTQUFRO0FBQUEsc0JBQUUsT0FBTTtBQUFBOztrQkFFM0ZELFlBQWUsc0JBQUE7QUFBQTs7Ozs7Ozs7Ozs7Ozs7In0=
